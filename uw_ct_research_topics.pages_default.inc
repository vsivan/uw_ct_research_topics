<?php
/**
 * @file
 * uw_ct_research_topics.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function uw_ct_research_topics_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'research_topics_page';
  $page->task = 'page';
  $page->admin_title = 'Research Topics Page';
  $page->admin_description = '';
  $page->path = 'research-topics-page';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Research Topics',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_research_topics_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'research_topics_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'images' => NULL,
      'title_and_description' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Research Topics';
  $display->uuid = 'ca10ffe8-090a-4890-b4d2-40a86ccf50be';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-bd28f500-27b4-4cde-9083-9a8ecebb207a';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'views-research_topics_view-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bd28f500-27b4-4cde-9083-9a8ecebb207a';
    $display->content['new-bd28f500-27b4-4cde-9083-9a8ecebb207a'] = $pane;
    $display->panels['middle'][0] = 'new-bd28f500-27b4-4cde-9083-9a8ecebb207a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-bd28f500-27b4-4cde-9083-9a8ecebb207a';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['research_topics_page'] = $page;

  return $pages;

}
