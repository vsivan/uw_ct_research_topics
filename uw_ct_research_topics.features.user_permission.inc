<?php
/**
 * @file
 * uw_ct_research_topics.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_research_topics_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create research_demos content'.
  $permissions['create research_demos content'] = array(
    'name' => 'create research_demos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create research_topics content'.
  $permissions['create research_topics content'] = array(
    'name' => 'create research_topics content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any research_demos content'.
  $permissions['delete any research_demos content'] = array(
    'name' => 'delete any research_demos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any research_topics content'.
  $permissions['delete any research_topics content'] = array(
    'name' => 'delete any research_topics content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own research_demos content'.
  $permissions['delete own research_demos content'] = array(
    'name' => 'delete own research_demos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own research_topics content'.
  $permissions['delete own research_topics content'] = array(
    'name' => 'delete own research_topics content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any research_demos content'.
  $permissions['edit any research_demos content'] = array(
    'name' => 'edit any research_demos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any research_topics content'.
  $permissions['edit any research_topics content'] = array(
    'name' => 'edit any research_topics content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own research_demos content'.
  $permissions['edit own research_demos content'] = array(
    'name' => 'edit own research_demos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own research_topics content'.
  $permissions['edit own research_topics content'] = array(
    'name' => 'edit own research_topics content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter research_demos revision log entry'.
  $permissions['enter research_demos revision log entry'] = array(
    'name' => 'enter research_demos revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter research_topics revision log entry'.
  $permissions['enter research_topics revision log entry'] = array(
    'name' => 'enter research_topics revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos authored by option'.
  $permissions['override research_demos authored by option'] = array(
    'name' => 'override research_demos authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos authored on option'.
  $permissions['override research_demos authored on option'] = array(
    'name' => 'override research_demos authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos promote to front page option'.
  $permissions['override research_demos promote to front page option'] = array(
    'name' => 'override research_demos promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos published option'.
  $permissions['override research_demos published option'] = array(
    'name' => 'override research_demos published option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos revision option'.
  $permissions['override research_demos revision option'] = array(
    'name' => 'override research_demos revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_demos sticky option'.
  $permissions['override research_demos sticky option'] = array(
    'name' => 'override research_demos sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics authored by option'.
  $permissions['override research_topics authored by option'] = array(
    'name' => 'override research_topics authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics authored on option'.
  $permissions['override research_topics authored on option'] = array(
    'name' => 'override research_topics authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics promote to front page option'.
  $permissions['override research_topics promote to front page option'] = array(
    'name' => 'override research_topics promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics published option'.
  $permissions['override research_topics published option'] = array(
    'name' => 'override research_topics published option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics revision option'.
  $permissions['override research_topics revision option'] = array(
    'name' => 'override research_topics revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override research_topics sticky option'.
  $permissions['override research_topics sticky option'] = array(
    'name' => 'override research_topics sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search research_demos content'.
  $permissions['search research_demos content'] = array(
    'name' => 'search research_demos content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search research_topics content'.
  $permissions['search research_topics content'] = array(
    'name' => 'search research_topics content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
