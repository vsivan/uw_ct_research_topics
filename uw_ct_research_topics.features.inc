<?php
/**
 * @file
 * uw_ct_research_topics.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_research_topics_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_research_topics_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_research_topics_node_info() {
  $items = array(
    'research_topics' => array(
      'name' => t('Research Topics'),
      'base' => 'node_content',
      'description' => t('A Research Demo at VIP Lab. Holds information regarding the research topic with supplementary figures. Also holds references to related publications, related demos and VIP lab members.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
